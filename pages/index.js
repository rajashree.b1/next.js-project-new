import Head from 'next/head';

// navbar component
import Navbar from "../components/Navbar";

// import home css
import styles from "../styles/Home.module.css";

export default function Home(){
  return(
    <div>

      <Head>
        <title>Home App</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />

        <link 
          href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;500;600;700;800&display=swap" 
          rel="stylesheet" /> 

      </Head>

      <header>
        <Navbar />
      </header>
      <div className={styles.typography}>
          <div className="c_container">
              <div className="c_row mb_5">
                <h4 className="m_auto Dark_color w_100 underline">Display 1 :</h4>
                  <h1 className="m_auto w_100 display_text1">Hello
                      <span className="display_text2 Primary_1">Jumbaya</span>
                  </h1>
              </div>
              <div className="c_row">
                <h4 className="m_auto Dark_color w_100 underline">Heading 1 :</h4>
                <h1 className="m_auto w_100">Jumbaya</h1>

                <h4 className="m_auto Dark_color w_100 underline">Heading 2 :</h4>
                <h2 className="m_auto Primary_1 w_100">Verification</h2>

                <h4 className="m_auto Dark_color w_100 underline">Heading 3 :</h4>
                <h3 className="m_auto Primary_1 w_100">Verification 1</h3>

                <h4 className="m_auto Dark_color w_100 underline">Heading 4 :</h4>
                <h4 className="m_auto Primary_1 w_100">Verification 2</h4>

                <h4 className="m_auto Dark_color w_100 underline">Body 1 :</h4>
                <p className="body1 mb_5">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat sunt mollitia, doloremque consectetur officia molestiae aut quas doloribus hic sapiente sit rerum consequatur quis laboriosam quibusdam ad repudiandae voluptate autem.</p>

                <h4 className="m_auto Dark_color w_100 underline">Body 2 :</h4>
                <p className="m_auto body2 w_100 mb_5">Lorem ipsum dolor</p>

                <h4 className="m_auto Dark_color w_100 underline">Text Styles Button :</h4>
                <button className={styles.submitbtn} type="button">Next</button>

                <h4 className="m_auto mt_5 Dark_color w_100 underline">Text Styles Small :</h4>
                <p className="small w_100">Lorem ipsum dolor</p>

                <h4 className="m_auto Dark_color w_100 underline">Text Styles Indicator Messages :</h4>
                <p className="indi_msgs w_100">Lorem ipsum dolor</p>
              </div>
          </div>
      </div>
    </div>
  )
}
